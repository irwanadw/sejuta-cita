import express from "express";
import { getUserProfile, UserLogin } from "../controllers"
import { Authenticate } from "../middlewares";

const router = express.Router();

router.get('/', (req, res) => {
    res.json({ message : "Welcome to User Route"})
})

router.post('/login', UserLogin)
router.get('/profile', Authenticate, getUserProfile)

export { router as UserRoute };