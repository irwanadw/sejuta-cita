import express from "express";
import { AdminLogin, createAdmin, createUser, deleteUser, getUser, getUserById, updateUser } from "../controllers";
import { Authenticate } from "../middlewares";

const router = express.Router();

router.get('/', (req, res) => {
    res.json({ message : "Welcome to Admin Route"})
})


router.get('/user', Authenticate, getUser)
router.get('/user/:id', Authenticate, getUserById)
router.post('/user', Authenticate, createUser)
router.post('/register', createAdmin)
router.post('/login', AdminLogin)
router.patch('/user/profile', Authenticate, updateUser)
router.delete('/user/profile', Authenticate, deleteUser)

export { router as AdminRoute };