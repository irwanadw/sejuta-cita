export interface CreateAdminInput {
    name: string;
    employeeNumber: number;
    email: string;
    address: string;
    phone: string;
    password: string;
    role: string;
}

export interface AdminLoginInput {
    employeeNumber: number;
    password: string;
}

export interface AdminPayload {
    _id: string;
    name: string;
    employeeNumber: number;
    role:string;
}