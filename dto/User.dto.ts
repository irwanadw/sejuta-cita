export interface CreateUserInput {
    firstName: string;
    lastName: string;
    username: string;
    address: string;
    phone: string;
    email: string;
    password: string;
    age: number;
    role: string;
}

export interface UserLoginInput {
    username: string;
    password: string;
}

export interface UserPayload {
    _id: string;
    username: string;
    email: string;
    role: string;
}

export interface EditUser {
    firstName: string;
    lastName: string;
    username: string;
    address: string;
    phone: string;
    password: string;
    age: number;
}

export interface DeleteUser {
    username: string;
    email: string;
}