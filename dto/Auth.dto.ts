import { UserPayload } from './User.dto'
import { AdminPayload } from './Admin.dto'

export type AuthPayload = UserPayload | AdminPayload;