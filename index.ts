import express from "express";
import App from './services/ExpressApp';
import dbConnection from './services/Databases';

const StartServer = async() => {
    const app = express()
    await dbConnection()
    await App(app)

    app.listen(3000, () => {
        console.log("I Love You 3000")
    });
}

StartServer();


// import bodyParser from 'body-parser';
// import mongoose from "mongoose";

// import { AdminRoute, UserRoute } from "./routes"
// import { MONGO_URI } from "./config";

// const app = express();

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));


// mongoose.connect(MONGO_URI, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//     useCreateIndex: true
// }).then((result) => {
//    console.log("DB Connected");
    
// }).catch((err) => {
//     console.log('error'+ err);
        
// });;


// app.use('/admin', AdminRoute);
// app.use('/user', UserRoute);
// app.use('/', (req, res) => {
//     return res.json('Welcome in Sejuta Cita Task Backend!')
// })

// app.listen(3000, () => {
//     console.log("Server running in port 3000")
// })