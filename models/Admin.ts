import mongoose, { Schema, Document, Model } from "mongoose";

interface AdminDoc extends Document {
    name: string;
    employeeNumber: number;
    email: string;
    phone: string;
    password: string;
    salt: string;
    role: string;
}

const AdminSchema = new Schema({
    name: { type: String, required: true},
    employeeNumber: { type: String, required: true},
    email: { type: String, required: true},
    phone: { type: String, required: true},
    password: { type: String, required: true},
    salt: { type: String, required: true},
    role: { type: String, required: true},
}, {
    toJSON: {
        transform(doc, ret) {
            delete ret.password;
            delete ret.salt;
            delete ret.__v;
            delete ret.createdAt;
            delete ret.updatedAt;
        }
    },
    timestamps: true
});

const Admin = mongoose.model<AdminDoc>('admin', AdminSchema)

export { Admin }