import mongoose, { Schema, Document, Model } from "mongoose";

interface UserDoc extends Document {
    firstName: string;
    lastName: string;
    username: string;
    address: string;
    phone: string;
    email: string;
    password: string;
    salt: string;
    profilPicture: string;
    age: number;
    role: string;
}

const UserSchema = new Schema({
    firstName: { type: String, required: true},
    lastName: { type: String, required: true},
    username: { type: String, required: true},
    address: { type: String, required: true},
    phone: { type: String, required: true},
    email: { type: String, required: true},
    password: { type: String, required: true},
    salt: { type: String, required: true},
    profilPicture: { type: String},
    age: { type: Number },
    role: { type: String, required: true},
},{
    toJSON: {
        transform(doc, ret) {
            delete ret.password;
            delete ret.salt;
            delete ret.__v;
            delete ret.createdAt;
            delete ret.updatedAt;
        }
    },
    timestamps: true
});

const User = mongoose.model<UserDoc>('user', UserSchema);

export { User } 