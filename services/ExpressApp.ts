import express, { Application } from "express";
import bodyParser from 'body-parser';


import { AdminRoute, UserRoute } from "../routes"

export default async(app: Application) => {
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    
    
    app.use('/admin', AdminRoute);
    app.use('/user', UserRoute);
    app.use('/', (req, res) => {
        return res.json('Welcome in Sejuta Cita Task Backend!')
    })

    return app;
}

