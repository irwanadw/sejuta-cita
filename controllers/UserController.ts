import { Request, Response, NextFunction} from "express"
import { findUser } from './AdminController';
import { UserLoginInput } from '../dto';
import { GenerateSignature, ValidatePassword } from '../utility';

export const UserLogin = async(req: Request, res: Response, next: NextFunction) => {
    const { username, password } = <UserLoginInput> req.body;
    const  existingUser = await findUser('', username);
    // console.log(password, "INI PASWORD");
    // console.log(existingUser, "INI EXISTING ID");
    // console.log(existingUser?.password, "INI EXIST PASWORD");
    // console.log(existingUser.salt, "INI SALT");

    if(existingUser !== null) {
        
        //validation and give access
        const validation = await ValidatePassword(password, existingUser?.password, existingUser?.salt);
        
        if (validation) {
            const signature = GenerateSignature({
                _id: existingUser?.id,
                username: existingUser?.username,
                email: existingUser?.email,
                role: existingUser?.role
            })

            return res.json({
                "message": `Login Success, welcome back ${existingUser?.firstName   }`,
                "loginToken": signature
            })
        } else {
            return res.json({"message" : "Password is not valid"})
        }
    }
    return res.json({"message": "Login credential not valid"})
}

export const getUserProfile =  async(req: Request, res: Response, next: NextFunction) => {
    
    const user = req.user;

    if(user) {
        const existingUser = await findUser(user._id)
        
        return res.json(existingUser)
    }
    
    return res.json({"message": "Vandor information Not Found"})
}