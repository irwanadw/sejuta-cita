import { Request, Response, NextFunction} from "express"
import { AdminLoginInput, CreateAdminInput, CreateUserInput, DeleteUser, EditUser } from "../dto"
import { Admin, User } from "../models";
import { GeneratePassword, GenerateSalt, GenerateSignature, ValidatePassword } from "../utility";

/** Admin Credential */
export const createAdmin = async(req: Request, res: Response, next: NextFunction) => {
    const { name, employeeNumber, email, address, phone, password} = <CreateAdminInput>req.body;

    const existingAdminNumber = await Admin.findOne({ employeeNumber: employeeNumber})

    if(existingAdminNumber !==null) {
        return res.json({"message": `Admin with employee number ${existingAdminNumber.employeeNumber} already registered`})
    }
//generate salt    
    const  salt = await GenerateSalt();
    const  adminPassword = await GeneratePassword(password, salt);

    const createdAdmin = await Admin.create({
        name: name,
        employeeNumber: employeeNumber,
        email: email,
        address: address,
        phone: phone,
        salt: salt,
        password: adminPassword,
        role: "admin"
    })
    return res.json(createdAdmin)
}

export const findAdmin = async(id: string | undefined, employeeNumber?:number) => {
    if(employeeNumber) {
        return await Admin.findOne({employeeNumber: employeeNumber})
    } else {
        return await Admin.findById(id)
    }
}

export const AdminLogin  = async(req: Request, res: Response, next: NextFunction) => {
    const { employeeNumber, password } = <AdminLoginInput>req.body;
    const existingAdmin = await findAdmin('', employeeNumber);

    if(existingAdmin !== null) {
        const validation = await ValidatePassword(password, existingAdmin?.password, existingAdmin?.salt);
        
        if(validation) {
            const signature = GenerateSignature({
                _id: existingAdmin?.id,
                employeeNumber: existingAdmin?.employeeNumber,
                name: existingAdmin?.name,
                role: existingAdmin?.role
            })

            return res.json({
                "message": `Login Success, Happy work ${existingAdmin?.name}`,
                "loginToken" : signature
            })
        } else {
            return res.json({"message": "Password is not valid"})
        }
    }
    return res.json({"message": "Login credential is not valid"})
     
}


/** Admin Task */
export const findUser = async(id: string | undefined, username?:string) => {
    if(username) {
        return await User.findOne({ username: username})
    } else {
        return await User.findById(id)
    }
}

export const createUser = async (req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName, username, address, phone, email, password, age} = <CreateUserInput>req.body;
    const user = req.user
    try {
        if(user?.role == "admin") {
            const existingUserName = await User.findOne({ username: username})
            const existingUserEmail = await User.findOne({ email: email })
            if(existingUserName  !== null || existingUserEmail !== null ) {
                return res.json({ "message": "A user is exist with this username or email ID" })
            }
            //generate salt
            
            const salt = await GenerateSalt()
            const userPassword = await GeneratePassword(password, salt);
            //encrypt the password using the salt
            const createdUser = await User.create({
                firstName: firstName,
                lastName: lastName,
                username: username,
                address: address,
                phone: phone,
                email: email,
                password: userPassword,
                salt: salt,
                age: age,
                role: "user"
            })
        
            return res.status(201).json({
                "message" : "Register Success",
                "registered data" : createdUser
            })
            
        } else {
            return res.status(403).json({"message" : "Only Admin can register the user data"})
        }
    } catch (error) {
        return res.status(500).json({"message": error})
    }

    
}

export const getUser = async (req: Request, res: Response, next: NextFunction) => {

    const users = await User.find()

    if(users !== null) {
        return res.json(users)
    }

    return res.json({ "message": "user data doesn't available"})

}

export const getUserById = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.params.id

    const user = await findUser(userId)

    if(user !== null) {
        return res.json(user)
    }

    return res.json({"message": "users data not available"})
}

export const updateUser =  async(req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName, username, password, address, phone} = <EditUser>req.body;
    const user = req.user
    const userUpdate = await User.findOne({username: username})
    // console.log(user);
    
    // console.log(user?.role, "INI ROLEEEE");
    // console.log(user, "INI DATA USER");
    try { 
        if (user?.role == "admin") {
            if(userUpdate !== null) {
                    userUpdate.address = address;
                    userUpdate.phone = phone;
                    userUpdate.firstName = firstName;
                    userUpdate.lastName = lastName;
                    userUpdate.password = password;
        
                    const result = await userUpdate.save()
                    return res.status(201).json({
                        "message": "Edit Success",
                        "Edited data" :result})
            } else {
                return res.status(404).json({"message" : "User information Not Found"})
            }   
        } else {
            return res.status(403).json({"message" : "Only Admin can updated the user data"})
        }
    } catch (error) {
        return res.status(500).json({"message": error})
    }
}

export const deleteUser =  async(req: Request, res: Response, next: NextFunction) => {
    const { username, email } = <DeleteUser>req.body;
    const user = req.user
    const userDelete = await User.findOne({username: username})

    try { 
        if (user?.role == "admin") {
            if(userDelete !== null) {
                    const deletedData = await userDelete.deleteOne()
                    return res.status(200).json({
                        "message": "Deleted Success",
                        "deleted data": deletedData
                    })
            } else {
                return res.status(404).json({"message" : "User information Not Found"})
            }   
        } else {
            return res.status(403).json({"message" : "Only Admin can delete the user data"})
        }
    } catch (error) {
        return res.status(500).json({"message": error})
    }
}
