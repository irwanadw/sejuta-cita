#task detail
1. Dengan NodeJS, agar saudara membuat Rest API CRUD User dan User Login.
2. Framework dibebaskan, tetapi disarankan ExpressJS. Database bebas, tetapi disarankan MongoDB.
3. User Login digunakan user (username, password) untuk mengakses API CRUD (token, tetapi mendapatkan nilai tambahan jika menggunakan refresh token).
5. Bikin 2 users dengan role: 1 Admin, 1 User.
6. Admin bisa melakukan/mengakses semua API CRUD, sedangkan User hanya bisa mengakses data user bersangkutan saja (Read)
7. Implementasi arsitektur Microservices, menggunakan Kubernetes dengan Docker container deploy di VPS (1 node dengan beberapa pod di dalamnya). Bagi yang belum memiliki VPS, maka cukup (a) menyiapkan semua YML agar aplikasi bisa dijalankan secara containerize dan siap di deploy di Kubernetes dan (b) di-deploy di lokal dan sertakan screenshoot. 
8. Upload source code ke Github beserta script YML Kubernetes.
9. Bikin dokumentasi API nya (Postman atau Swagger) yang bisa diakses ke server Rest API nya.
10. Bikin diagram arsitektur nya yang menjelaskan flow API CRUD dan Login.
11. Lampirkan credential Admin di Readme.

Mohon submit kesini (biasanya orang kirim link Github) paling lambat 17 Juli 2021.

Terima kasih :)